function [Xo, Yo] = coldftreg(V, X, Y, K)
%COLDFTREG align the columns of a matrix X with a column reference vector
%Xr using the correlation method. If a third matrix Y is provided, use the
% estimated shift from (X, V) to align Y as well.
%   [Xo,~] = COLDFTREG(V, X) where Xr is a column vector. X is a column vector
%   or a matrix with the same number of rows similar to that of X. Return
%   the column-aligned Xo version of X.
%   [Xo,Yo] = COLDFTREG(V, X, Y) uses the estimated shift when aligning X
%   to align Y. The aligned result of Y is Yr.
%   [Xo,Yo] = COLDFTREG(V, X, Y, K) aligns X and Y using an upsampling factor
%   of K. If not specified, K = 1.
%   Examples: please see demo.m
%   Author: Tan H. Nguyen,
%   Research Lab of Electronics, EECS, MIT.
%   thnguyn@mit.edu
%   Version history:
%   V1.0: first implementation, support only the case where length(X) =
%       size(X,1).
%   V2.0: support flexible dimension where the length of V can be smaller
%       than size(X, 1). Handle the case where the number of rows in X is
%       odd. Add mean subtraction to improve stability.
    if (nargin < 2)
        error('There is nothing to align, please provide at least 2 arguments');
    elseif (nargin < 4)
        usFact = 1;
    else
        usFact = K;
    end
    
    % Make sure that the dimension are consistent
    V = V(:);
    if (length(V) > size(X, 1))
        error('Dimension requirement not satisfied. Please make sure that length(V) <= size(X, 1)')
    end
   
    N = size(X, 1);

    % Make sure the number of rows in X is even. 
    if (mod(N, 2) == 1)
        error('Please make sure the number of rows in X is even');
    end
    
    % Subtract V by the mean
    V = V - mean(V);
    X = X - repmat(mean(X, 1), [N 1]);
    
    lV = length(V);
    Vpad = zeros(N, 1);
    Vpad(1 : lV) = V(:);
    
    Xo = [];
    Yo = [];
    N1 = N * usFact;
    Ncols = size(X, 2);
    % Estimate the amount of shift from each column of X to align with V.
    Xf = fft(X, [], 1);
    Vf = fft(Vpad, [], 1);
    VRepf = repmat(Vf, [1 Ncols]);
    crossSpecDens = Xf .* conj(VRepf);  % Low-res cross-spectral density.
    crossSpecDensUs = zeros(N1, Ncols);
    crossSpecDensUs(1 : N / 2, :) = crossSpecDens(1 : N / 2, :);
    crossSpecDensUs(end - N/2 + 1 : end, :) = crossSpecDens(N / 2 + 1 : end, :);
    gamma = fftshift(ifft(crossSpecDensUs, [], 1), 1);
    [~, maxIdx2] = max(abs(gamma), [], 1);
    colShift = maxIdx2 -  N1 / 2 - 1;  % Amount of shift to be done for each column of X.
    
    % Shift by the amount of shift amount
    shiftMat = repmat(colShift, [N 1]);
    
    % Generate a phase term to multiply with the spectrum of X and Y to
    % shift it.
    vIdx = [0 : (N/2 - 1) (N1 - N/2) : (N1 - 1)];     % Because the output signal is not upsample, this is only to N.
    vIdxMat = repmat(vIdx(:), [1 Ncols]);
    % Shift each sweep by the same amount.
    phaseMap = exp(2i * pi / N1 * vIdxMat .* shiftMat);
    Xof = Xf .* phaseMap;
    Xo = real(ifft(Xof, [], 1));
    
    % Truncate the dimensions to output a fraction that matches with the
    % length of V.
    Xo = Xo(1 : lV, :);
    
    % Further shift Y if Y is provided
    if (nargin > 3)
        if (~isempty(Y))
            if (length(V) > size(Y, 1))
                error('Dimension requirement not satisfied. Please make sure that length(V) <= size(Y, 1)')
            end
            Y = Y - repmat(mean(Y, 1), [N 1]);
            Yf = fft(Y, [], 1);
            Yof = Yf .* phaseMap;
            Yo = real(ifft(Yof, [], 1));
            % Truncate the output to output a fraction that matches with the
            % length of V.
            Yo = Yo(1 : lV, :);
        end
    end

end