% This source code demonstrates the use of the function COLDFTREG to 
% register different columns of a matrix X to a reference vector V.
% This function should be useful applications where there is
% signal-to-signal variation in the columns of X.
clc;
close all;
clear all;

% Read an example image of X.
X = imread('X.tiff');
Y = imread('Y.tiff');

%% Example 1: align every column of X with a fraction of its first column.
% Output the fraction that is aligned with this reference
[Xo, ~] = coldftreg(X(18 : 563, 1), X, [], 16);
figure(1);
subplot(1, 2, 1); 
imagesc(X(400 : 500, 300 : 400)); colormap gray; title('X before registration');
subplot(1, 2, 2); 
imagesc(Xo(400 : 500, 300 : 400)); colormap gray; title('X after registration (Xo)');


%% Example 2: align both X and Y using the estimation from X
[Xo, Yo] = coldftreg(X(18 : 563, 1), X, Y, 16);
figure(2);
subplot(1, 2, 1); 
imagesc(Y(400 : 500, 300 : 400)); colormap gray; title('Y before registration');
subplot(1, 2, 2); 
imagesc(Yo(400 : 500, 300 : 400)); colormap gray; title('Y after registration (Xo)');

